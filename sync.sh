#!/bin/sh

if [ $# -eq 0 ]
  then
    echo "Usage: sync.sh shader.glsl ip"
else

  SHADER=$1  
  IP=$2
  curl --data-binary "@${SHADER}" http://${IP}:7341/config/playback/user_shader

fi
