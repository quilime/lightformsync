#!/bin/sh

if [ $# -eq 0 ] 

then

  echo "Usage: sync.sh shader.glsl ip"

else

  SHADER=$1  
  IP=$2

  fswatch -0 -or ${1} | xargs -0 -n 1 -I {} ./sync.sh ${SHADER} ${IP}

fi
